//const url="https://api.iextrading.com/1.0/"

const url="https://sandbox.iexapis.com/v1/"
const token='Tpk_f359bc401a5446f4b6876cffd821e22c'
import {axios} from "./Api.js"

function getMostActive(){
  return axios.get(url+"stock/market/list/mostactive?token="+token)
}

function getGainers(){
  return axios.get(url+"stock/market/list/gainers?token="+token)
}

function getLosers(){
  return axios.get(url+"stock/market/list/losers?token="+token)
}

function getMostVolumne(){
  return axios.get(url+"stock/market/list/iexvolume?token="+token)
}

function getStockKeyStats(symbol){
  return axios.get(url+'stock/'+symbol+'/stats?token='+token)
}

function getStockChart(symbol){
  return axios.get(url+"stock/"+symbol+"/chart/1m?token="+token)
}

function getStockQuote(symbol){
  return axios.get(url+"stock/"+symbol+"/quote?token="+token)
}

function getStockNews(companyName){
  return axios.get("https://newsapi.org/v2/everything?q="+companyName+"&apiKey=3fa466295e9043db952ed5220a30bfa2")
}

function getBatchQuote(stocks){
  var query=stocks.join()
  return axios.get(url+"/stock/market/batch?symbols="+query+"&types=quote&token="+token)
}
export default {
  getMostActive,getGainers,getLosers,getMostVolumne,
  getStockKeyStats,getStockChart,getStockQuote,getStockNews,
  getBatchQuote
}
