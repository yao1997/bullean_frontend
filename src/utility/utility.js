/* eslint-disable */

function extractString(string){
  var div = document.createElement('div')
  div.innerHTML=string;
  var text= div.textContent;
  return text;
}

function checkEmailFormat(email){
  return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)
}
export {extractString,checkEmailFormat}
