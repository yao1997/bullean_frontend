

function setAuthCookie(data,app){
  app.$cookies.config('30d')
  app.$cookies.set('firstName',data.firstName)
  app.$cookies.set('lastName',data.lastName)
  app.$cookies.set('email',data.email)
  app.$cookies.set('id',data.id)
  console.log(data.token)
  app.$cookies.set('token',data.token)
}

function getAllCookies(app){
  var result={}
  if (app.$cookies.keys().length>0){
    app.$cookies.keys().forEach(function(el){
      result[el]=app.$cookies.get(el)
    })
    return result
  }
  return null
}

function removeAllCookie(app){
  app.$cookies.keys().forEach(function(el){
    app.$cookies.remove(el)
  })
}
export {setAuthCookie,getAllCookies,removeAllCookie}
