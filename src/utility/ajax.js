const cc_url="https://min-api.cryptocompare.com/data"
const axios = require('axios');

function get(url,data,success,err){
  var req_url=!data?url:url+"?"+generateQuery(data)
  axios.get(req_url).then(success).catch(err)
}

function generateQuery(data){
  var str = [];
  for (var q in data)
    if (data.hasOwnProperty(q)) {
      str.push(encodeURIComponent(q) + "=" + encodeURIComponent(data[q]));
    }
  return str.join("&");
}

export {get}
