 /* eslint-disable */
//const url="http://128.199.90.65:81"
//const url="http://localhost:3000"
const url=process.env.VUE_APP_URL
const axios = require('axios');
import {app} from "../main.js"

axios.create({
  withCredentials: true
})
axios.interceptors.request.use(function(config) {
  app.$store.commit("changeLoading",true)
  return config;
}, function(error) {
  app.$store.commit("changeLoading",false)
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  setTimeout(function(){
    app.$store.commit("changeLoading",false)
  },200)
  return response;
}, function (error) {
  console.log(error)
  app.$store.commit("changeLoading",false)
  return Promise.reject(error.response);
});

function configureHeader(token){
  axios.interceptors.request.use(config => {
    config.headers.post['token'] = token;
    config.headers.delete['token'] = token;
    config.headers.put['token'] = token;
    return config;
  });
}

function removeHeader(){
  axios.interceptors.request.use(config => {
    //config.headers={}
    return config
  });
}

function auth(data){
  console.log("running")
  return axios.post(url+"/authentication/login",data)
}

function getProfile(token){
  return axios.get(url+"/user/getProfile",{ headers: { token }} )
}

function register(data){
  return axios.post(url+"/authentication/register",data)
}

function searchStockByName(name){
  return axios.get(url+"/stock/searchByName/"+name)
}

function searchStockBySymbol(symbol){
  return axios.get(url+"/stock/searchBySymbol/"+symbol)
}

function getAndUpdateStock(symbol){
  return axios.get(url+"/stock/"+symbol)
}

function getStockComment(id){
  return axios.get(url+"/comment/getStockComment/"+id)
}

function createComment(data){
  return axios.post(url+"/comment/createComment",data,{withCredentials:true})
}

function replyComment(data){
  return axios.post(url+"/comment/replyComment",data,{withCredentials:true})
}

function deleteComment(data){
  return axios.delete(url+"/comment/deleteComment",{data:data},{withCredentials:true})
}

function updateComment(id,data){
  return axios.put(url+"/comment/editComment/"+id,data,{withCredentials:true})
}

function getPortfolio(token){
  return axios.get(url+"/portfolio/getPortfolio",{ headers: { token } })
}

function addStockToPortfolio(data){
  return axios.put(url+"/portfolio/addStock",data)
}

function removeStockFromPortfolio(data){
  return axios.put(url+"/portfolio/removeStock",data)
}

function updateProfile(data){
  return axios.put(url+"/user/updateProfile",data)
}

function oAuth(code){
  return axios.post(url+"/authentication/oAuth",{code})
}

function updatePassword(data){
  return axios.put(url+"/authentication/changePassword",data)
}

export {auth,register,searchStockByName,searchStockBySymbol,
  getAndUpdateStock,getStockComment,createComment,configureHeader,
  replyComment,axios,deleteComment,updateComment,addStockToPortfolio,removeHeader,
  getPortfolio,removeStockFromPortfolio,getProfile,updateProfile,oAuth,updatePassword}
