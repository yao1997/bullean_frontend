import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import News from "./pages/News"
import Stock from "./pages/Stock"
import Main from "./pages/Main"
import Profile from "./pages/Profile"
import store from "./store/index"
import VueApexCharts from 'vue-apexcharts'
import VueCookies from 'vue-cookies'
import Portfolio from "./pages/Portfolio"
import GAuth from 'vue-google-oauth2'

Vue.config.productionTip = false
Vue.prototype.$eventHub=new Vue();
Vue.use(VueRouter);
Vue.use(VueApexCharts)
Vue.use(VueCookies)
Vue.use(GAuth, {
  clientId: '974746454042-ekagtqsd073t33id4a4gsunqteci5qe0.apps.googleusercontent.com',
  scope: 'profile email https://www.googleapis.com/auth/plus.login'
})
Vue.component('apexchart', VueApexCharts)
const routes=[
  {path:"/",component:Main},
  {path:"/news",component:News},
  {path:"/portfolio",component:Portfolio},
  {path:"/stock/:stockSymbol",component:Stock},
  {path:"/profile",component:Profile}
]

const router=new VueRouter({routes})

var app=new Vue({
  render: h => h(App),
  iconfont: 'mdi',
  data:()=>({
    currentNews:{},
    currentCrypto:{},
    scroll:{
      duration:500,
      offset:0,
      easing:"easeInQuad"
    }
  }),
  store,
  router
}).$mount('#app')

app.$cookies.config('1d')

export {app}
