/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import iex from "../utility/iex"

Vue.use(Vuex)
const axios = require('axios');
const ajax = require("../utility/ajax.js")
const googleNews_url="https://newsapi.org/v2/everything"
const apiKey="3fa466295e9043db952ed5220a30bfa2"
const api=require("../utility/Api.js")
const setAuthCookie=require("../utility/cookie.js").setAuthCookie
const getAllCookies=require("../utility/cookie.js").getAllCookies
const removeAllCookie=require("../utility/cookie.js").removeAllCookie

const store=new Vuex.Store({
  state: {
    authState:null,portfolio:null,loading:false,loadingCount:0,
    currentNewsList:[],hotNewsList:[],newsPages:0,
    loginModal:false,logoutModal:false,
    loginModalResgisterState:false,drawer:false,
    alert:{message:"",type:"success",value:false},
    main_stock:null,main_stock_chart:null,main_stock_quote:null,main_stock_indices:null
  },
  mutations: {
    auth(state,data){
      state.authState=data.data
      setAuthCookie(data.data,data.app)
      console.log(data.data.token)
      api.configureHeader(data.data.token)
    },

    checkAuth(state,app){
      if (app.$cookies.get("token")){
        state.authState=getAllCookies(app)
        api.configureHeader(app.$cookies.get("token"))
      }
    },

    updateProfile(state,profile){
      console.log("called")
      state.authState={...state.authState,...profile}
    },

    logout(state,app){
      removeAllCookie(app)
      api.removeHeader()
      state.authState=null
    },

    updatePortfolio(state,portfolio){
      state.portfolio=portfolio
    },

    changeLoading(state,val){
      val?state.loadingCount+=1:state.loadingCount-=1
      state.loading=state.loadingCount>0
    },

    displayAlert(state,data){
      state.alert={...data,value:true}
      setTimeout(function(){
        state.alert.value=false
      },3000)
    },

    getNews(state,page){
      const data={sources:"business-insider",language:"en",pageSize:10,page,apiKey}
      const that=this;
      ajax.get(googleNews_url,data,response=>{
        that.state.currentNewsList=response.data.articles
        that.state.newsPages=parseInt(response.data.totalResults/10)-1
      },err=>{
        console.log(err)
      })
    },

    getBusinessNews(){
      const url="https://newsapi.org/v2/top-headlines?sources=cnbc,the-wall-street-journal&apiKey=3fa466295e9043db952ed5220a30bfa2"
      const that=this
      ajax.get(url,null,response=>{
        that.state.hotNewsList=response.data.articles
      },err=>{
        console.log(err)
      })
    },

    triggerLoginModal(){
      this.state.loginModal=!this.state.loginModal
    },

    triggerLogoutModal(){
      this.state.logoutModal=!this.state.logoutModal
    },

    triggerSideMenu(){
      this.state.drawer=true
    },

    removeBrokenImage(state,title){
      this.state.currentNewsList=this.state.currentNewsList.filter(news=>news.title!=title)
    },

    updatePortfolio(state,portfolio){
      this.state.authState.portfolio=portfolio
    },

    getStockKeyStats(state,symbol){
      iex.getStockKeyStats(symbol).then(response=>{
        console.log(response.data.companyName)
        state.main_stock={...this.main_stock,...response.data,symbol}
      }).catch(err=>{
        console.log(err)
      })
    },

    getStockChart(state,symbol){
      iex.getStockChart(symbol).then(response=>{
        state.main_stock_chart=response.data
      }).catch(err=>{
        console.log(err)
      })
    },

    getStockIndices(state,symbol){
      api.getAndUpdateStock(symbol).then(response=>{
        state.main_stock_indices=response.data.data
      }).catch(err=>{
        console.log(err)
      })
    },

    getStockQuote(state,symbol){
      iex.getStockQuote(symbol).then(response=>{
        state.main_stock_quote={...response.data}
      }).catch(err=>{
        console.log(err)
      })
    },

    clearStock(state){
      state.main_stock=null
      state.main_stock_chart=null
      state.main_stock_quote=null
      state.main_stock_indices=null
    }
  }
})

export default store
