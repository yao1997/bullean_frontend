
# Bullean (Frontend)

Bullean is a web app that helps investors to do stock fundamental analysis. After so, user can add stock into portfolio and check the portfolio performance.

Bullean is constructed using nodejs(express), mongodb and vuejs. The backend is hosted on a DigitalOcean (https://www.digitalocean.com/) droplet whereas the frontend is hosted with FreeHostia (https://www.freehostia.com/)

Bullean get all of the stock data from IEX Trading api (https://iextrading.com/developer/docs/), after which bullean will perform a series of calculation to analyze stocks.



## Before we start

There are a few things that you might want to check out :
1. Youtube Video :
2. Website link: http://edward97.com/bullean
3. Bitbucket Repository : https://bitbucket.org/yao1997/bullean/src/master/



## Things I have done:
- [x] Hosted : On http://edward97.com
- [x] Environment Variables for Development and Production
- [x] Server side CORS setting.
- [x] Material Design : Using vuetify and apexchart.
- [x] Repo Usage : with consistent commits
- [x] Different Branches in Repo : Master and Development
- [x] Input Validation
- [x] OAuthentication : Google Account Login
- [x] 3 Third Parties API request (News,Stock and oAuth)
- [x] Tools Usage in Dev experience
- [x] Well organized code and file structure.
- [x] Highly Component-Based Code Structure for reuse purpose

## Tools Used
1. Atom Text Editor (https://atom.io/)
2. Windows cli
3. Postman (https://www.getpostman.com/)
4. MongoDB Compass (https://www.mongodb.com/products/compass)
5. Putty (https://www.putty.org/)
6. MCedit (http://www.mcedit.net/)
7. Filezilla (https://filezilla-project.org/)
8. Sourcetree (https://www.sourcetreeapp.com/)
9. Robo 3T (https://robomongo.org/)

## Dependencies

1.  Vuetify (https://vuetifyjs.com/en/layout/pre-made-themes)
2.  Vue-Router (https://router.vuejs.org/)
3.  Vuex (https://vuex.vuejs.org/)
4.  Epic Spinner (https://github.com/epicmaxco/epic-spinners)
5.  Vue ApexChart (https://apexcharts.com/docs/vue-charts/)
6.  Sass-Loader (https://github.com/webpack-contrib/sass-loader)
7.  vue-cookies (https://www.npmjs.com/package/vue-cookies)
8.  vue-google-oauth2 (https://www.npmjs.com/package/vue-google-oauth2)
9.  Axios (https://www.npmjs.com/package/axios)



## Installation
If you are willing to clone the project. Kindly pull the repository.
```
git clone https://yao1997@bitbucket.org/yao1997/bullean_frontend.git```
```

Then install the dependencies (Make sure that you have nodejs and npm installed properly)
```
npm install
```
And the project can be run via the command
```
npm run serve
```

## TODO
1.  Responsive Design.
2.  Enhanced Portfolio feature.
3.  OAuth with FB account.
4.  Profile Page Analysis (Based on past data)
5.  Auto generated email (For registration and user)
6.  More robusting analysis on stocks.
7.  Home page enhancements, with more introductions about Bullean


## Data Flow Diagram
![alt text](https://i.ibb.co/nBjrcQc/DFD.png "Level 1 Data Flow Diagram")

## Use Case Diagram
![alt text](https://i.ibb.co/TLj6R7d/Use-Case.png "Level 1 Data Flow Diagram")
